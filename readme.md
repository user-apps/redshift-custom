Made for linux

This software will write and replace the content of the file `~/.config/redshift.conf`.

# Before running

```bash
sudo apt install python3-tk python3-pip
pip install customtkinter
```

The software `redshift` must **NOT** be configured to run at startup!

# Use it

```bash
./redshift-custom.py
```

Run `redshift-custom.py` to customize the blue light and brightness of redshift.

```bash
./redshift-custom-start.py
```

The script `redshift-custom-start` can be run at startup of the computer. It will just activate the last configuration saved with `redshift-custom`.