#!/usr/bin/python3
#coding:utf-8

import os

def file_exist(file_name):
    return os.path.exists(file_name)

def load_file(path):
    if not file_exist(path):
        print("error file not found")
    else:
        load = []
        with open(path, 'r') as file:
            for line in file:
                load.append(line.strip('\n'))
    return load

def write_file(path, content):
    with open(path, 'w') as file:
        file.write(content)

path=os.path.expanduser('~') + "/.config/redshift.conf"
temperature=6500
brightness=1.0

if not file_exist(path):
    content="""[redshift]
temp-day=6500
temp-night=6500
brightness-day=1.0
brightness-night=1.0"""
    write_file(path, content)
else:
    config=load_file(path)
    for i in config:
        tmp = i.split('=')
        if tmp[0]=="temp-day":
            temperature=int(tmp[1])
        elif tmp[0]=="brightness-day":
            brightness=float(tmp[1])

os.system(f"redshift -P -O {temperature} -b {brightness}")