#!/usr/bin/python3
#coding:utf-8

import customtkinter
import os

#load data from ~/.config/redshift.conf

path=os.path.expanduser('~') + "/.config/redshift.conf"
temperature=6500
brightness=1.0

def file_exist(file_name):
    return os.path.exists(file_name)

def load_file(path):
    if not file_exist(path):
        print("error file not found")
    else:
        load = []
        with open(path, 'r') as file:
            for line in file:
                load.append(line.strip('\n'))
    return load

def write_file(path, content):
    with open(path, 'w') as file:
        file.write(content)

#check file

if not file_exist(path):
    content="""[redshift]
temp-day=6500
temp-night=6500
brightness-day=1.0
brightness-night=1.0"""
    write_file(path, content)
else:
    config=load_file(path)
    for i in config:
        tmp = i.split('=')
        if tmp[0]=="temp-day":
            temperature=int(tmp[1])
        elif tmp[0]=="brightness-day":
            brightness=float(tmp[1])

class App(customtkinter.CTk):
    def __init__(self):
        super().__init__()

        #window config

        customtkinter.set_appearance_mode("dark")  # Modes: "System" (standard), "Dark", "Light"
        customtkinter.set_default_color_theme("green")  # Themes: "blue" (standard), "green", "dark-blue"

        self.geometry("560x280")
        self.title("redshift custom")
        self.minsize(560, 280)

        #grid config

        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure((0, 1), weight=1)

        #frames

        self.frame_1 = customtkinter.CTkFrame(master=self)
        self.frame_1.grid(row=0, column=0, pady=20, padx=20, sticky="nsew")

        self.frame_2 = customtkinter.CTkFrame(master=self)
        self.frame_2.grid(row=0, column=1, pady=20, padx=20, sticky="nsew")

        #frame_1 temperature

        self.label1_1 = customtkinter.CTkLabel(master=self.frame_1, justify=customtkinter.LEFT, text="temperature", font=("monospace", 21))
        self.label1_1.pack(pady=10, padx=10)

        self.label1_2 = customtkinter.CTkLabel(master=self.frame_1, justify=customtkinter.LEFT, text=f"current: {temperature}", text_color="gray", font=("monospace", 14))
        self.label1_2.pack(pady=10, padx=10)

        self.slider1 = customtkinter.CTkSlider(master=self.frame_1, command=self.slider1_callback, from_=1500, to=6500, number_of_steps=10)
        self.slider1.pack(pady=10, padx=10)
        self.slider1.set(temperature)

        self.label1_3 = customtkinter.CTkLabel(master=self.frame_1, justify=customtkinter.LEFT, text=temperature, font=("monospace", 21))
        self.label1_3.pack(pady=10, padx=10)

        #frame_2 brightness

        self.label2_1 = customtkinter.CTkLabel(master=self.frame_2, justify=customtkinter.LEFT, text="brightness", font=("monospace", 21))
        self.label2_1.pack(pady=10, padx=10)

        self.label2_2 = customtkinter.CTkLabel(master=self.frame_2, justify=customtkinter.LEFT, text=f"current: {brightness}", text_color="gray", font=("monospace", 14))
        self.label2_2.pack(pady=10, padx=10)

        self.slider2 = customtkinter.CTkSlider(master=self.frame_2, command=self.slider2_callback, from_=0.1, to=1.0)
        self.slider2.pack(pady=10, padx=10)
        self.slider2.set(brightness)

        self.label2_3 = customtkinter.CTkLabel(master=self.frame_2, justify=customtkinter.LEFT, text=brightness, font=("monospace", 21))
        self.label2_3.pack(pady=10, padx=10)

        #button

        self.button = customtkinter.CTkButton(master=self, command=self.button_callback, text="Apply")
        self.button.grid(row=1, column=0, columnspan=2, padx=10, pady=10)

        #set colors
        self.slider1_callback(temperature)
        self.slider2_callback(brightness)

    #functions
    def slider1_callback(self, value):
        global temperature
        temperature = int(value)
        self.label1_3.configure(text=temperature)
        if temperature > 4500:
            self.slider1.configure(progress_color="blue")
            self.label1_3.configure(text_color="blue")
        elif temperature > 3000:
            self.slider1.configure(progress_color="magenta")
            self.label1_3.configure(text_color="magenta")
        else:
            self.slider1.configure(progress_color="red")
            self.label1_3.configure(text_color="red")
    
    def slider2_callback(self, value):
        global brightness
        brightness = float("%.1f"%value)
        self.label2_3.configure(text=brightness)
        if brightness > 0.7:
            self.slider2.configure(progress_color="yellow")
            self.label2_3.configure(text_color="yellow")
        elif brightness > 0.5:
            self.slider2.configure(progress_color="white")
            self.label2_3.configure(text_color="white")
        elif brightness > 0.3:
            self.slider2.configure(progress_color="lightgray")
            self.label2_3.configure(text_color="lightgray")
        else:
            self.slider2.configure(progress_color="gray")
            self.label2_3.configure(text_color="gray")

    def button_callback(self):
        content=f"""[redshift]
temp-day={temperature}
temp-night={temperature}
brightness-day={brightness}
brightness-night={brightness}"""
        write_file(path, content)
        os.system(f"redshift -P -O {temperature} -b {brightness}")
        self.label1_2.configure(text=f"current: {temperature}")
        self.label2_2.configure(text=f"current: {brightness}")

if __name__ == "__main__":
    app = App()
    app.mainloop()